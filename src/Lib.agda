module Lib where

mutual
  Zaybe : (a b : Set) → Set
  Zaybe a b = Maybe (b × a)

  ZaybeII = Zaybe Int Char × String

  data Decl : Set where
          D1 : Decl
          D2 : Decl
  {-# FOREIGN GHC Decl = data Decl (D1 | D2) #-}

  data Expr : Set where
          E1 : Zaybe Int String → Expr
          E2 : Expr
          E3 : List ZaybeII → Expr
          E4 :   Maybe (Maybe (Bool ×  Bool) ×  (Maybe (Char ×  Int) ×  String)) → Expr
          E5 : Maybe (Int ×  Bool) → Decl → Expr

  {-# FOREIGN GHC Expr = data Expr (E1 | E2 | E3 | E4 | E5) #-}
