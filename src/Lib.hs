module Lib  where

type Zaybe a b = Maybe (b,a)

type ZaybeII = (Zaybe Int Char,String)

type ZaybeIII = Zaybe (ZaybeII) (Zaybe Bool Bool)

type ZaybeIV x = Zaybe Bool x
-- Maybe ((Maybe Int Int,String),(Maybe (Bool,Bool)))

data Decl =
    D1
  | D2


data Expr  =
    E1 (Zaybe Int String)
  | E2
  | E3 [ZaybeII]
  | E4 [[ZaybeIII]]
  | E5 (ZaybeIV Int) Decl

someFunc :: IO ()
someFunc = putStrLn "someFunc"



-- Maybe (Maybe Int → Int) → String) → (Maybe Bool → Bool))
