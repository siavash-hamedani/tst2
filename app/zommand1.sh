#!/bin/bash

MBASE="ZEXE"
BN="$(echo $1 | perl -pe 's|\.|D|pg')"
BNM1="$(echo $1 | tr '.' '/')"
BNM="$(dirname $BNM1 | tr '/' '.')"

cat Mtst4.hs | perl -pe "s|Agda.Syntax.Concrete.Declaration|$1|pg" | perl -pe "s|=IMPORTME=|$BNM|pg" | perl -pe "s|=FULLFILLED=|$2$FULLFILLED|" | perl -pe "s|Mtst1|Ztst1"$BN"|pg" > $MBASE/Ztst1$BN.hs

cat $MBASE/Ztst1$BN.tmp | grep -v "=depends=" > ZTHOUT/$1".out"

stack runhaskell $MBASE/Ztst1$BN.hs > $MBASE/Ztst1$BN.tmp

grep "=depends=" $MBASE/Ztst1$BN.tmp | cut -f2 > $MBASE/Ztst1$BN.tmp2

FULLFILLED=\"$1\",$2

echo $FULLFILLED

cat $MBASE/Ztst1$BN.tmp2 | while read line
do
    FULLFILLED=$(./zommand1.sh $line $FULLFILLED)
    
done
