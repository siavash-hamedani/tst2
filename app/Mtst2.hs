{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards  #-}

module Mtst2 where



import qualified Data.Text as T

import Control.Monad.Trans.Except
import Control.Monad.State

import Data.List
import Data.Maybe

import Language.Haskell.TH

import qualified Data.Map as DM
import qualified Data.Set as DS

import System.FilePath

import Control.Monad

import Lib

type MySt = DS.Set String

type MyQ a = StateT MySt Q a

tshow :: (Show a) => a -> String
tshow = tail . takeExtension . show

prettyCustom (OrigName n) = tshow n
prettyCustom _ = ""

prettyLinearType (LinearType (CustomTuple2) [x1,x2]) = "(" ++ prettyLinearType x1 ++ " ×  " ++ prettyLinearType x2 ++ ")"
prettyLinearType (LinearType x xs) = prettyCustom x ++ " " ++ (intercalate " " $ map prettyLinearType xs)
prettyLinearType (LF x) = prettyCustom x


renderAgCon (AgCon {..}) = tshow agcName ++ " : " ++ (intercalate " → " $ map prettyLinearType agcArgCon)

renderAgD :: AgD -> String
-- renderAgD = show
renderAgD (AgD {..}) = "data " ++ tshow agdName ++ " : Set where\n\t" ++
  (intercalate "\n\t" $ ((map (\zz -> renderAgCon zz ++
                                case length $ agcArgCon zz of
                                  0 -> ""
                                  _ -> " → "
                                ++ (tshow agdName)) agdCons) ))
renderAgD _ = ""

render :: DS.Set AgD -> String
render x = intercalate "\n\n" $ map renderAgD $ DS.toList x

hask2ag'' :: String -> Q Exp
hask2ag'' typ = do
  Just tnam <- lookupTypeName typ
  let ppq = flip runStateT mempty $ hask2ag tnam
  pp2 <- runQ ppq
  let pp3 = DS.filter (\x -> not $ "GHC"  `isInfixOf` (show $ getAgDName x) ) $ snd pp2
  let pp = fixAgts (DS.toList pp3,[])
  let kk = fixAgds $ (partition (not . isAgT) pp,[])
--  let gg = (intercalate "\n\n" $ map show $ DS.toList pp3) ++ "\n\n----====----====---\n\n" ++ (render $ DS.fromList pp)
  let pp = kk
  let gg = (render $ DS.fromList pp)
  
  [e|gg|]


isAgT (AgT {..}) = True
isAgT _ = False



prepareTyp :: Type -> StateT (DS.Set AgD) Q ()
prepareTyp (ConT c) = hask2ag c
prepareTyp (ListT) = return ()
prepareTyp (VarT _) = return ()
prepareTyp (TupleT _) = return ()
prepareTyp (AppT a b) = do
  prepareTyp a
  prepareTyp b
prepareTyp t = error $ show ("preparetyp", t)

prepareCon :: Con -> StateT (DS.Set AgD) Q ()
prepareCon (NormalC cnam cbtypes) = do
  let typs = map snd cbtypes
  mapM prepareTyp typs
  hask2ag cnam
prepareCon t = error $ show ("prepareCon", t)


hask2ag :: Name -> StateT (DS.Set AgD) Q ()
hask2ag tnam = do
  rnam <- lift $ reify tnam

  case rnam of
    TyConI tycon -> do
      case tycon of
        DataD _ dnam tyvars _ cns _ -> do
          mapM prepareCon cns
          let a = AgD {agdName = dnam, agdArgs = tyvars, agdCons = map mkCon cns}
          pushDataInfo a
        NewtypeD _ dnam tyvars _ cn _ -> do
          prepareCon cn
          let a = AgD {agdName = dnam, agdArgs = tyvars, agdCons = [mkCon cn]}
          pushDataInfo a
        TySynD dnam tyvars ttyp -> do
          prepareTyp ttyp
          let a = AgT {agtName = dnam, agtArgs = tyvars, agtUnderlyingType = linearizeAppT ttyp}
          pushDataInfo a
        _ -> error $ show ("wow implement",tycon)
    PrimTyConI pnam _ _ -> do
      let a = AgPrim {agpName = pnam}
      pushDataInfo a
    DataConI _ _ _ -> do
      return ()
    _ -> error $ show ("wow2 implement",rnam)
  return ()

pushDataInfo :: AgD -> StateT (DS.Set AgD) Q ()
pushDataInfo a = do
  modify (\x -> DS.insert a x)

data AgCon = AgCon {agcName :: Name, agcImpli :: [String],agcArgCon :: [LinearType]} deriving Show

mkCon :: Con -> AgCon
mkCon (NormalC nm btyp) = AgCon {agcName = nm, agcImpli = [], agcArgCon = map (linearizeAppT . snd)  btyp}

data AgD =
    AgD {agdName :: Name , agdArgs :: [TyVarBndr], agdCons :: [AgCon]}
  | AgT {agtName :: Name , agtArgs :: [TyVarBndr], agtUnderlyingType :: LinearType}
  | AgPrim {agpName :: Name}
  deriving Show



getAgDName (AgD {..}) = agdName
getAgDName (AgT {..}) = agtName
getAgDName (AgPrim {..}) = agpName

data CustomName = OrigName Name | CustomTuple2 | CustomList deriving (Ord,Eq)

instance Show CustomName where
  show (OrigName x) = "O@" ++ show x
  show (CustomTuple2) = "T2@"
  show (CustomList) = "L@"
  

data LinearType = LinearType CustomName [LinearType] | LF CustomName

instance Show LinearType where
  show (LinearType x xs) = "(" ++ show x ++ ":: " ++ (intercalate " → " $ map show xs) ++ ")"
  show (LF x) = "||"++show x

linearizeAppT :: Type -> LinearType
linearizeAppT (VarT a) = LF $ OrigName a
linearizeAppT (ConT a) = LF $ OrigName a
linearizeAppT (TupleT 2) = LF $ CustomTuple2
linearizeAppT (ListT) = LF $ CustomList
linearizeAppT (AppT a b) = case linearizeAppT a of
  LinearType x xs -> LinearType x (xs++[linearizeAppT b])
  LF x -> LinearType x [linearizeAppT b]
linearizeAppT t = error $ show ("linearizeappt",t)

-- convertTypeToData :: AgD -> StateT (DS.Set AgD) Q AgD
-- convertTypeToData t@(AgD {..}) = return t
-- convertTypeToData t@(AgPrim {..}) = return t
-- convertTypeToData t@(AgT {..}) = do
--   undefined

instance Eq (AgD) where
  a == b = getAgDName a == getAgDName b
  
instance Ord (AgD) where
  compare (a) (b) = (compare (getAgDName a) (getAgDName b)) 
  

fixAgts :: ([AgD],[AgD]) -> [AgD]
fixAgts ([],x) = x
fixAgts (x@(AgT {..}):xs,y) = case containsAgT (agtUnderlyingType) xs of
  Just j ->  fixAgts (xs++[x],y)
  Nothing -> case containsAgT (agtUnderlyingType) y of
    Just k -> fixAgts ((x {agtUnderlyingType = fixUnderly  (DS.fromList y) k agtUnderlyingType}):xs,y)
    Nothing -> fixAgts (xs,x:y)
fixAgts (x:xs,y) = fixAgts (xs,x:y)

fixUnderly :: DS.Set AgD  -> Name ->  LinearType -> LinearType
fixUnderly db n o@(LinearType lt lts) | and $ (isJust $ n `customNameEq` lt):(map (\zz -> not $ isJust $ containsAgT zz $ DS.toList db) lts) = let a = head $ DS.toList $ DS.filter (\y -> getAgDName y == n) db in  jmap lts (agtUnderlyingType a) (map tyvarName $ agtArgs a)
                                      | otherwise = LinearType lt $  map (fixUnderly db n) lts
fixUnderly db n o@(LF lt) | isJust $ n `customNameEq` lt = let a = head $ DS.toList $ DS.filter (\y -> getAgDName y == n) db in  (agtUnderlyingType a)
                          | otherwise = o


tyvarName (PlainTV n)= n
tyvarName (KindedTV n _)=n

linearTypeName (LinearType n _) = n
customNameName (OrigName n)=n
customNameName t =error $ show t

jmap :: [LinearType] -> LinearType -> [Name] -> LinearType
jmap lts (LinearType x y) w = LinearType x (map (jmap2 db) y)
  where
    db = DM.fromList (zip w lts)

jmap2 :: DM.Map Name LinearType -> LinearType -> LinearType
jmap2 db (LinearType x y) = LinearType x $ map (jmap2 db) y
jmap2 db (LF x) = case DM.lookup (customNameName x) db of
  Nothing -> LF x
  Just j -> j


containsAgT :: LinearType ->  [AgD] -> Maybe Name
containsAgT a [] = Nothing
containsAgT o@(LF a) (x:xs) = case  customNameEq (getAgDName x) a of
  Just j -> Just j
  Nothing -> containsAgT o xs
containsAgT o@(LinearType a as) (x:xs) = case  filter isJust $ (customNameEq (getAgDName x) a):(map (flip containsAgT (x:xs)) as) of
  (Just k):_ -> Just k
  [] -> containsAgT o xs

customNameEq :: Name -> CustomName -> Maybe Name
customNameEq a (OrigName o) | a==o = Just o
customNameEq _ _ = Nothing

fixAgds :: (([AgD],[AgD]),[AgD]) -> [AgD]
fixAgds (([],y),x) = x++y
fixAgds ((x@(AgD {..}):xs,y),r) = case filter (isJust) $ tmp of
  [] -> fixAgds ((xs,y),x:r)
  ress -> fixAgds ((xs++[newx],y),r)
  where
    tmp = map (fixCon y) agdCons
    newx = x {agdCons = (map (nothingOrig) $ zip agdCons tmp)}
fixAgds ((x:xs,y),r) = fixAgds ((xs,y),x:r)


fixCon :: [AgD] -> AgCon -> Maybe AgCon
fixCon db o@(AgCon {..}) = case filter isJust tmp of
  [] -> Nothing
  _ -> Just $ o {agcArgCon = map nothingOrig $ zip agcArgCon $ tmp}
  where
    tmp = map (fixConArg db) agcArgCon

fixConArg :: [AgD] -> LinearType -> Maybe LinearType
fixConArg db o@(LF l) = case containsAgT o db of
  Nothing -> Nothing
  Just j -> let a = head $ DS.toList $ DS.filter (\y -> getAgDName y == j) (DS.fromList db) in  Just $ jmap [o] (agtUnderlyingType a) (map tyvarName $ agtArgs a) -- good
fixConArg db o@(LinearType l ls) = case containsAgT o db of
  Nothing -> Nothing
  Just j -> case filter isJust tmp of
    [] -> let a = head $ DS.toList $ DS.filter (\y -> getAgDName y == j) (DS.fromList db) in  Just $ jmap ls (agtUnderlyingType a) (map tyvarName $ agtArgs a) -- good
    _ -> Just $ LinearType l $ (map nothingOrig $ zip ls $ map (fixConArg db) ls)
    where
      tmp = map (flip containsAgT db) ls
nothingOrig :: (a, Maybe a) -> a
nothingOrig (x,Nothing) = x
nothingOrig (x,Just y) = y
