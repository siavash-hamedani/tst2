{-# LANGUAGE TemplateHaskell #-}
module Agda.Syntax.Ztst1LibDZaybe where



import Lib

import qualified Data.Text as T

import Control.Monad.Trans.Except
import Control.Monad.State

import Data.Data
import Data.List

import Control.Monad.State

import Language.Haskell.TH

import Mtst2

import Lib

import qualified Data.Set as DS

main :: IO ()
main = do
  let fullfilledini = DS.fromList ["Lib.Expr","GHC.Base.Maybe","GHC.Types.Bool","GHC.Base.String","Data.Set.Internal.Set","GHC.Integer.Type.Integer","Data.Strict.Maybe.Maybe"]
  
  putStrLn  $(fmap fst $  flip runStateT (mempty) $ hask2ag "Lib.Zaybe"   )
  let ll = DS.fromList $(hask2ag'' "Lib.Zaybe"   )

  -- putStrLn $ intercalate "\n" $ map (\x -> "-here- putStrLn $(fmap fst $  flip runStateT (mempty) $ hask2ag \""++x++"\"   )") ll
  let fullfilled = DS.insert "Lib.Zaybe" fullfilledini
  putStrLn $ intercalate "\n=depends=\t" $ DS.toList $ DS.difference ll fullfilled
